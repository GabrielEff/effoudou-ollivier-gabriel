---
# Complète ici tes informations au format YAML
# un doute sur la syntaxe ? aide toi du site http://www.yamllint.com/

# Tes informations de profil (INFORMATIONS OBLIGATOIRES)
prenom: Yahia
nom: Boubaker

# écrit ci-dessous ta présentation en markdown
# un doute sur la syntaxe ? aide toi des ressources disponible sur moodle
---

# profil
**prénom:** ***Yahia***

**nom:**  ***Boubaker***

![Photo](content/Boubaker-Yahia/image/12.png)

## contact 
e-mail: [yahiaboubaker216@gmail.com](mailto:yahiaboubaker216@gmail.com)

num de tel: _0623653867_

## formation
2020/2022: _Baccalauréat général_ 

2023/2024: _BTS SIO_

## compétences
| compétences | exemple | 
|:-|:-:|
| langue de code | Python |
| outils base de données | excel |
| Langues | Français, Anglais, Arabe |

## expériences
job étudiant: _équipier à Macdo_
