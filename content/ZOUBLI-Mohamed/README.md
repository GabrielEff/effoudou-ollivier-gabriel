---
# Complète ici tes informations au format YAML
# un doute sur la syntaxe ? aide toi du site http://www.yamllint.com/

# Tes informations de profil (INFORMATIONS OBLIGATOIRES)
prenom: Mohamed
nom: ZOUBLI

# écrit ci-dessous ta présentation en markdown
# un doute sur la syntaxe ? aide toi des ressources disponible sur moodle
---

# Profile

![alt text](data:image/)

-Je m'appelle ZOUBLI Mohamed, je suis en première année de BTS SIO au lycée la Coloniére, j'aime bien jouer au jeux-vidéo, lire et apprendre a programmer pour m'améliorer.


## Contact

- numèro de téléphone : 

- mail: [mohamedzoubli55@gmail.com](mailto:mohamedzoubli55@gmail.com)


- Réseaux sociaux professionnelle:
    - [Instagram]()
    - [Twitter]()
    - [Linkedin]()


# Compétence

| Compétence       | Exemple          
|:-|:-:|
| Langue de programmation |   Python, Htlm      
| langues                 |   Français, Anglais           
| système d'exploitation  |   Windows, Linux


# Expérience professionnelle

- Je n'ai pas beaucoup d'expérience professionnelle à part les deux stages que j'ai pu effectuer, un en 3ème dans un garage automobile et un deuxième que j'ai réaliser durant les vacances d'été dans un magasin de réparation de téléphone.
