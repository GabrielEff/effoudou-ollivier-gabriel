---
prenom: Marwane
nom: Dribek
---
# Profil
![photo](https://filerepository.itslearning.com/5f02f8b8-89b7-4f84-8876-8360cc8727dc?Token=dqMHAFoNAwAOqxVlAAAAACAAprd05G-wkpENh-EHJNtpfLcA09ueH5uEg3bTkhwVuB8AAA&Download=1)  
Je m'appelle Marwane Dribek j'ai 18 ans et je suis actuellement en première année de BTS SIO au Lycée La Colinière.  
## Contact
Numéro Téléphone:[06-38-42-99-42](tel:+330638429942),  
Email:[marwanedrbk@gmail.com](mailto:marwanedrbk@gmail.com)

# Formation
-**Seconde Générale** _Lycée Alcide D'orbigny Bouaye_ (2020/2021)  
-**Première STL** _Lycée Alcide D'orbigny Bouaye_ (2021/2022)  
-**Terminale STL** _Lycée Alcide D'orbigny Bouaye_ (2022/2023)  
-**BTS SIO** _Lycée La Colinière Nantes_ (2023/2024)  

# Compétences  

|  **Compétences**|**Connaissances**  |
|--|--|
|**Programmation**  |Python, HTML  |
|**langages**|Français, Anglais, Espagnol|
|**Système d'exploitation**|Windows, Linux|

# Experience professionelle
-Stage d'observation à la Maison De La Tranquilité publique de Rezé.

