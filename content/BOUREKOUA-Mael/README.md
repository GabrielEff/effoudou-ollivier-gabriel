---
# Complète ici tes informations au format YAML
# un doute sur la syntaxe ? aide toi du site http://www.yamllint.com/

# Tes informations de profil (INFORMATIONS OBLIGATOIRES)
prenom: Mael            
nom: Bourekoua

# écrit ci-dessous ta présentation en markdown
# un doute sur la syntaxe ? aide toi des ressources disponible sur moodle
---
# Profil

![Photo de moi](https://media.discordapp.net/attachments/1154070447525220424/1156946072405553242/fond-ecran-abstrait-noir-grunge-photo-gratuit_1340-33848.jpg?ex=6516d17e&is=65157ffe&hm=c3fd521bb2ce6b1d5c9ce21b509239c6e2385f37c99f27ca5bbf9883e07713a6&=)

BOUREKOUA Mael
19 ans
Etudiant en **BTS - Service Informatique aux Organisation** à La Colinière
>## Contact :
>>**Tel** : [0782810004](+33782810004)
>>**Mail** : [maelbourekoua.pro@gmail.com](maelbourekoua.pro@gmail.com)

# Compétences
|Compétences|Détails|
|-|-|
|**Programmation**|Python, C, SQL|
|**Système d'exploitation**|Windows, Ubuntu, Debian|
|**Langues**|Français, Anglais, Espagnol|
|**Atout**|Travail d'équipe, Autonome, Polyvalent|

# Formations
- 2019-2022 : **Bac Général** : Spécialité Mathématiques, NSI - *Lycée la Colinière*
- 2022-2023 : **L1 - Science de l'ingénieur** - *IUT de Nantes*
- 2023-2024 : **BTS - Service Informatique aux Organisation** - *Lycée la Colinière*

# Expérience Professionnelle 

2018-2019: **Stage de 3ème en Restauration** - *Jardin d'Arcadie*

- Préparation de repas en fonction du menu
- Service à table
- Travail d'équipe





