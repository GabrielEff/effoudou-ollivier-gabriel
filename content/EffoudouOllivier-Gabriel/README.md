---
prenom: Gabriel
nom: Effoudou-Ollivier
---

# Profil :

![](media/Effoudou-Gabriel.jpg)    
Je m'appelle Gabriel Effoudou-Ollivier, j'ai 19ans et je suis étudiant en 1ère année de BTS SIO (Services Informatiques aux Organisations) à Nantes.

## Contacts :

Téléphone : [0633032937](tel:+33633032937)  
e-mail : [gabrieleffoll@gmail.com](mailto:gabrieleffoll@gmail.com)

# Formation :

- **BTS :** - SIO (Services Informatiques aux Organisations), 1ère année (Nantes,2023-2024),    
spécialité envisée : SLAM (Solutions Logicielles et Applications Métiers)   
- **Licence Biologie** - accès santé (Rennes, 2022-2023)    
- **Terminale générale**, spécialités : mathématiques, SVT (Rennes, 2021-2022)  
- **Première générale**, spécialités : mathématiques, physique/chimie et SVT (Rennes,2020-2021)     
- **Seconde générale** , (Rennes, 2019-2020)       

# Experiences :

- Stage d'observation chez un opticien (Rennes, 2019)  
- Stage d'observation laboratoire d'astrologie à Nantes (2019)  
- Jobs étudiants : babysitting, agent d'entretien

## Experience informatique :  
  
- **Apprentissage du language Pyton :**  
Python est le premier language informatique que j'ai eu l'occasion d'apprendre, j'ai beaucoup aimé le fait que l'on puisse créer des programmes avec une grande liberté.  
- **Découverte et apprentissage de l'HTML :**  
L'HTML m'a permis de mieux comprendre l'architecture numérique des sites web et je trouve ça passionant.
- **Réalisation dun trombinoscope sur un site internet :**  
Cette expérience m'a permis d'apprendre la rigueur notamment le respect de consignes établies dans le cahier des charges du projet.



# Compétences  
## Compétences techniques :

|compétences| connaissances |
|--|--|
|**Programmation**| python, markdown |
|**Systèmes d'exploitation**| Windows, Linux |
|**Langues vivantes**| Français, Anglais, Allemand|  

## Compétences transversales :
- Rigueur  
- Organisation  
- Capacité d'adaptation  
- Esprit d'équipe  
- Solide niveau d'anglais (écrit et oral)  
- Bases solides en Allemand (écrit et oral)  
- Permis B
