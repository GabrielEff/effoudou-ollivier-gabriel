---
# Complète ici tes informations au format YAML
# un doute sur la syntaxe ? aide toi du site http://www.yamllint.com/

# Tes informations de profil (INFORMATIONS OBLIGATOIRES)
prenom: Dorian
nom: Robin

# écrit ci-dessous ta présentation en markdown
# un doute sur la syntaxe ? aide toi des ressources disponible sur moodle
---

# Profil


Dorian Robin ![](https://favforward.com/app/uploads/2017/03/03.png)

## Contact:


e-mail: rdorian329@gmail.com Numéro: 0768331056

# Formation


*   2022-2023: Bac Technologique Lieu: _Châteaubriant_  
    
*   2023-2024: BTS SIO Lieu: _Nantes_
  
# Compétences

    
|  Compétences     |   Logiciel    |       
|---    |:-:    |
|    Programme   |    Python   |      
|   Système    | Windows      |       
    
# Expérience

    
Été 2023: Job d'été FMGC _Soudan_ ; Secteur: Maintenance

